package nhl.NhlSalaryDb

import nhl.SparkEnvironment._
import nhl.NhlApiFullPlayerIdList._
import nhl.TransformerImplicits.implicits._
import nhlDataQuality.SalaryValidation._
import net.ruippeixotog.scalascraper.browser.Browser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import net.ruippeixotog.scalascraper.model._
import scala.util.{Try,Success,Failure}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import java.util.Date
import java.time.Year
import com.typesafe.config.ConfigFactory
import java.io.File
import config.ConfigLoader._
import com.typesafe.config.Config

/**
  * Create csv with Nhl players on active rosters, their cap hits, salary, agents and contract status
  */
object NhlSalaryDb extends App with NhlApiFullPlayerIdList with SparkEnvironment {
  // Set up headless browser
  import spark.implicits._
  import net.ruippeixotog.scalascraper.browser.JsoupBrowser
  
  // set spark log level... the metals logs still seem to be printing "INFO " as well
  sc.setLogLevel("WARN")
  val PATH = System.getProperty("user.dir")

  // Assign config paths
  //val profileArgs: String = args(0) // option to use profileArgs at a later date
  val salaryConfig: Config = loadConfig()
  val puckpediaUrl: String = salaryConfig.getString("nhl-salary-db.puckpedia.salary-input-site")
  val salaryOutputDirectory: String = salaryConfig.getString("nhl-salary-db.output.salary-output-path")
  val salaryOutputPath: String = s"$PATH/$salaryOutputDirectory"
  println(s"Base Working Directory: $PATH")
  println(s"Input Location: $puckpediaUrl")
  println(s"Output Locaitn: $salaryOutputPath")

  // Declare browser
  val browser: Browser = JsoupBrowser()

  // createDF
  val inputDf: Dataset[Row] = htmlTableToDf(browser, puckpediaUrl)

  // FilterDataframe
  val salaryDf = inputDf
    .transform(splitColumns)
    .transform(addAdditionalContractInfo)
  
  // Create output dataframe
  val outputDf: Dataset[Row] = salaryDf
    .transform(joinAndFilterSalaryWithActiveRoster(getRoster))
    .withColumn("season", lit("20202021"))

  // before we can write, should make sure that this passes the Deequ tests
  runSalaryDfVerificationSuite(outputDf)

  // Write output
  outputDf.writeCsvOutput(salaryOutputPath)
  //outputDf.show(20, false)

  /**
    * There are two sebastian aho's in the league, so after joining roster data to the salary data 
    * there's some additional formatting that needs to be done. Since salary datay doesn't have the official 
    * nhl api player ID we need to join on player names and then remove the excess aho combinations.
    *
    * @param roster active roster df
    * @param df salary df
    * @return joined and filtered salary-roster df
    */
  def joinAndFilterSalaryWithActiveRoster(roster: Dataset[Row])(df: Dataset[Row]): Dataset[Row] = {

    // Add to filter as necessary
    // TODO at some point this can be moved to a filter conf file.
    // this would avoid repackaging the project if there's another player with an identical name
    val filterCriteria = Seq("Mika Rautakallio8480222Carolina Hurricanes",
      "Claude Lemieux8478427Carolina Hurricanes",
      "Claude Lemieux8478427Carolina Hurricanes",
      "Claude Lemieux8478427New York Islanders",
      "Mika Rautakallio8480222New York Islanders")

    // join Salary with active roster from nhl api
    val mergeDf = df.join(roster, Seq("playerName"), "left")
    
    // remove every element in the filter criteria via a fold left operation
    filterCriteria.foldLeft(mergeDf){(mergeDf, f) => mergeDf
      .filter(concat($"playerAgent",$"playerId".cast("string"), $"teamName") =!= f)}
   }

  /**
    * Add an _ concat column of player name for future data processing.
    *
    * @param df dataframe
    * @return dataframe with additional column
    */
  def addPlayerId(df: Dataset[Row]): Dataset[Row] = {
    df
      .withColumn("pid", trim(lower($"playerName")))
  }

  /**
    * Add more information to contract details by adding whether the player signed a standard contract
    * as an RFA
    *
    * @param df clean DF
    * @return df with additional contract info
    */
  def addAdditionalContractInfo(df: Dataset[Row]): Dataset[Row] = {
    // get year for contract calc purposes
    val currentYear: Int = Year.now().toString().toInt

    df
      .withColumn("contractType", 
        when($"contractBegin".cast("int") < $"ufaYear".cast("int") && 
            lower($"contractType") === "standard", "standard_rfa")
          .otherwise(when(trim(lower($"contractType")).contains("entry level"), "entry_level")
            .otherwise("standard_ufa")))
  }

  /**
    * Return a dataframe from the input headless browser
    * 
    * - This function is probably a bit too big, definitely need to rethink at some point
    *
    * @param browser jsoup browser
    * @return dataframe
    */
  def htmlTableToDf(browser: Browser, path: String): Dataset[Row] = {  
    // get browser page
    val page = browser.get(path)

    // parse rows - this would get the first table it sees in the html
    val htmlRows = page >> elementList("tr")

    // split rows and table head
    val (tableHead, tableValues) = htmlRows.splitAt(1)

    // extract head
    val tableHeadExtract = tableHead >> texts("th")

    // extract values
    val tableValuesExtract = tableValues >> texts("td")

    // assert that it's not empty
    def notEmptyList(toAssert: List[Iterable[String]]): Try[Boolean] = Try(toAssert.isEmpty)
    
    // match try success failure
    notEmptyList(tableValuesExtract) match {
      case Success(value) => tableValuesExtract
      case Failure(exception) => println("failed, list empty")
    }

    // get content of success
    val concatContent =  Success(tableHeadExtract ::: tableValuesExtract).get

    // Content output - move to function
    val (columnNames, transformedSalaryList) = concatContent.splitAt(1)
    val content = transformedSalaryList.map(x => x.toList)

    content.toDF()
  }
  
  /**
    * Access the values within the dataframe that we need.
    * 
    * Player position was removed because they're different from the NHL api. 
    * So official api positions are being used
    *
    * @param df single column df
    * @return multi-column df
    */
  def splitColumns(df: Dataset[Row]): Dataset[Row] = {
    df
      .select(col("value").getItem(1).as("playerName"),
        col("value").getItem(3).as("currentCapHit"),
        col("value").getItem(4).as("currentSalary"),
        col("value").getItem(5).as("contractLength"),
        col("value").getItem(13).as("ufaYear"),
        col("value").getItem(14).as("contractType"),
        col("value").getItem(15).as("expiryStatus"),
        col("value").getItem(16).as("contractBegin"),
        col("value").getItem(17).as("contractEnd"),
        col("value").getItem(18).as("playerAgent")
      )
  }
}
