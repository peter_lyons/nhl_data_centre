package nhl

import DateValidator._
import SparkEnvironment._
import NhlDataTransformers._

object RunSparkApp extends App 
  with DateValidator 
  with SparkEnvironment 
  with NhlDataTransformers
  {

  import java.time.format.DateTimeFormatter
  import java.time.LocalDate
  import org.apache.spark.sql.functions._
  import org.apache.spark.sql.Dataset
  import org.apache.spark.sql.Row

  // Import Spark Implicits (for using the $ operator)
  import spark.implicits._

  //val date: String = args(0) // uncomment before build
  val date: String = "20210101" // for debugging

  // verify input date is valid, program exit if not
  validateDate(date)

  val inputDate = formatDate(date)

  // load team data
  val inputTeamsString: String = "file:/Users/peter/Downloads/teams.csv"
  val inputTeamsDf = spark.read.option("header", true).csv(inputTeamsString)

  // load skater data
  val inputSkaterString: String = "file:/Users/peter/Downloads/skaters.csv"
  val inputSkaterDf: Dataset[Row] = spark.read.option("header", true).csv(inputSkaterString)
  
  // load line data
  val inputLinesString: String = "file:/Users/peter/Downloads/lines.csv"
  val inputLinesDf: Dataset[Row] = spark
    .read
    .option("header", true)
    .csv(inputLinesString)

  //prep line data
  val preppedLinesDf: Dataset[Row] = inputLinesDf
    .transform(prepLinesData)

  preppedLinesDf.show(10, false)

  // prep skater data
  val preppedSkaterDf: Dataset[Row] = inputSkaterDf
    .transform(prepSkaterData)

  inputTeamsDf.printSchema()
  inputSkaterDf.printSchema()
  inputLinesDf.printSchema()

  //inputDf.printSchema()
  //inputDf.show(10, false)

  val linesPlayerIdMatch: Dataset[Row] = preppedLinesDf
    .transform(mergeLinesWithSkater(preppedSkaterDf))

  linesPlayerIdMatch.printSchema()
  //linesPlayerIdMatch.show(20, false)

  val viewQuery: Dataset[Row] = linesPlayerIdMatch
    .transform(viewPlayerBestLine)
    //.filter($"team"==="TOR")

  viewQuery.show(30, false)

  /*
  viewQuery
    .repartition(1)
    .write.format("csv")
    .option("header", "true")
    .save("file:/Users/peter/Documents/bestLines.csv")
  */

  /*
  viewQuery.write
  .format("org.apache.spark.sql.redis")
  .option("table", "nhlLinesData")
  .save()
*/
  val readRedis = spark
    .read
    .format("org.apache.spark.sql.redis")
    .option("table", "nhlLinesData")
    //.option("key.column", "name")
    .load()
  
  readRedis.show(10, false)
  //  println("save Complete")
 // val outDf: Dataset[Row] = inputTeamsDf
  //  .transform(viewXGoalsLeaders)


  //outDf.show(10, false)
  //println(inputDate.plusDays(1))

  /**
    * Formats the input date string as LocalDate after input string has been validated
    *
    * @param d input date as String
    * @return input date as LocalDate
    */
  def formatDate(d: String): LocalDate = {
    val dateFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd")
    LocalDate.parse(d, dateFormat)
  }
}
