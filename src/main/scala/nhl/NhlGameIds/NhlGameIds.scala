package nhl.NhlGameIds

import nhl.SparkEnvironment._

/**
  * Create db containing all NHL game id's for a season
  */
object NhlGameIds extends App with SparkEnvironment {
  import scalaj.http._
  import spark.implicits._
  import net.ruippeixotog.scalascraper.browser.JsoupBrowser

  val gameIds: HttpResponse[String] = Http("http://live.nhl.com/GameData/SeasonSchedule-20202021.json")
    .header("content-type", "application/json")
    .asString
  
  val gameInfo: String = gameIds.body
  //val gameIdDf = spark.read.json(Seq(gameInfo).toDS())
    //.withColumn("date", )

  val gameStatus = Http("https://statsapi.web.nhl.com/api/v1/game/2020020003/feed/live?site=en_nhl")
    .asString
  val gameOut = gameStatus.body

  val gameIdDf = spark.read.json(Seq(gameOut).toDS())
    //.withColumn("date", )

  val moneyPuckResponse: HttpResponse[String] = Http("http://moneypuck.com/g.htm?id=2020020001")
    .asString

  val moneyPuckInfo = moneyPuckResponse.body

  val browser = JsoupBrowser()
  //val doc = browser.parseFile("core/src/test/resources/example.html")
  val doc2 = browser.get("http://moneypuck.com/index.html?date=2021-05-27")

  //println(gameOut)
  
  //val test = Http("http://peter-tanner.com/moneypuck/jsonData/202020212020020001.json").asString
  //println(test.body)

//  println(moneyPuckInfo)
  gameIdDf.show(10)
  //gameIdDf.printSchema

  gameIdDf.select("gameData.teams.home.abbreviation", "gameData.teams.away.abbreviation").show()

  //"https://statsapi.web.nhl.com/api/v1/people/8477939/?stats=statsSingleSeason&season=20202021"
}
