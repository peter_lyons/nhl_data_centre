package nhl.NhlApiFullPlayerIdList

import nhl.SparkEnvironment._
import config.ConfigLoader._
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import scala.util.{Try,Success,Failure}
import org.apache.spark.sql.types.StructType
import nhl.TransformerImplicits.implicits._

trait NhlApiFullPlayerIdList extends SparkEnvironment {
  import scalaj.http._
  import spark.implicits._
  
  def getNhlTeamInformation(): (Dataset[Row], String) = {
    // get dataframe of nhl teams
    val nhlTeamsApiPath: String = loadConfig().getString("nhl-api.input.nhl-api-base-team-path")
    val nhlTeams: Dataset[Row] = returnJsonAsDataFrame(nhlTeamsApiPath, "teams").transform(cleanTeamInformation)
    (nhlTeams, nhlTeamsApiPath)
  }
  /**
    * Wrapper for creating dataframe of all active players on team rosters.
    * Includes player and team information from the NHL API
    *
    * @return dataset
    */
  def getRoster(): Dataset[Row] = {
    
    val (nhlTeams, nhlTeamsApiPath) = getNhlTeamInformation()

    // Extract the teamID from the dataset of NHL teams, create a list
    val m: Map[Char, String] = Map('[' -> "[", ']' -> "]")
    val nhlTeamSeq: List[Int] = nhlTeams
      .select($"teamId")
      .collect()
      .toList
      .map(x => x.toString.filterNot(e => m.contains(e)).toInt)

    // get dataframe of all active nhl rosters
    val nhlRosterApiPath: String = s"/roster"
    val nhlRoster: Dataset[Row] = nhlTeamSeq
    .map(i => tryJsonRosterPath(s"$nhlTeamsApiPath/$i$nhlRosterApiPath", i))
    .reduce(_ union _)
    
    // returns
    nhlRoster
    .join(nhlTeams, Seq("teamId"), "inner")
      
  }
  
  /**
    * 
    *
    * @return
    */
  def returnEmptyRosterDf(): Dataset[Row] = {
    // get empty dataframe schema from known team
    val nhlRosterPath: String = loadConfig().getString("nhl-api.input.nhl-api-base-roster-path")
    var nhlRosterSchema: StructType = returnJsonAsDataFrame(nhlRosterPath,
     "roster").transform(cleanRosterInformation(1)).schema
    spark.createDataFrame(sc.emptyRDD[Row], nhlRosterSchema)
  }

  /**
    * 
    *
    * @param url
    * @param i
    * @return
    */
  def tryJsonRosterPath(url: String, i: Int): Dataset[Row] = {
    try {
      returnJsonAsDataFrame(url, "roster").transform(cleanRosterInformation(i))
    } catch {
      case e: Throwable => returnEmptyRosterDf()
    }
  }

  /**
    * Explode json of lists into dataframe
    *
    * @param df json dataframe
    * @return clean dataframe of team information
    */
  def cleanTeamInformation(df: Dataset[Row]): Dataset[Row] = {
    def removeChars = (teamId: String) => {
      val m = Map('[' -> "[", ']' -> "]")
      teamId.filterNot(e => m.contains(e)).toInt
    }

    val charReplace = spark.udf.register("charReplaced", removeChars)
    
    df
      .select(charReplace(col("teams.id")).cast("int").as("teamId"), 
        col("teams.name").as("teamName"))

  }

  /**
    * 
    *
    * @param teamId
    * @param df
    * @return
    */
  def cleanRosterInformation(teamId: Int)(df: Dataset[Row]): Dataset[Row] = {
    df
      .select($"roster.person.fullName".as("playerName"),
        $"roster.person.id".as("playerId"),
        trim(lower($"roster.position.abbreviation")).as("playerPosition"))
      .withColumn("teamId", lit(teamId))

  }
  
  /**
    * 
    *
    * @param url
    * @param endpoint
    * @return
    */
  def returnJsonAsDataFrame(url: String, endpoint: String): Dataset[Row]  = {  
    val jsonFile: String = Http(s"$url")
    .header("content-type", "application/json")
    .asString
    .body

    spark.read.json(Seq(jsonFile).toDS()).explodeDataframeTransform(endpoint)

  }
}
