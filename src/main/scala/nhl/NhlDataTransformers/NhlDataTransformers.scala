package nhl.NhlDataTransformers

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import nhl.SparkEnvironment._

trait NhlDataTransformers extends SparkEnvironment {
  import org.apache.spark.sql.functions._
  import spark.implicits._

  /**
    * Get xGoalLeaders from moneypuck data
    *
    * @param df moneypuck data
    * @return aggregated dataset
    */
  def viewXGoalsLeaders(df: Dataset[Row]): Dataset[Row] ={
      df
       .groupBy("name",
      "season"
      )
    .agg(
      mean($"xGoalsPercentage").as("xGoalsPercentage"),
      mean($"corsiPercentage").as("corsiPercentageß")
    )
    .orderBy(desc("xGoalsPercentage"))
  }

  /**
    * Prepare moneypuck lines data
    *
    * @param df moneypuck lines data
    * @return reduced dataset
    */
  def prepLinesData(df: Dataset[Row]): Dataset[Row] = {
    df
      .select($"lineId",
       lower($"name").as("lineCombo"),
       lower($"team").as("lineTeam"),
       $"position",
       $"situation",
       $"icetime".as("lineIcetime"),
       $"xGoalsPercentage",
       $"xGoalsFor",
       $"games_played")
  }

  /**
    * Adds additional information from moneypuck player data
    *
    * @param df moneypuck skater data
    * @return df
    */
  def prepSkaterData(df: Dataset[Row]): Dataset[Row] = {
    df
      .withColumn("fName", lower(substring_index($"name", " ", 1)))
      .withColumn("lName", lower(substring_index($"name", " ", -1)))
      .withColumnRenamed("icetime", "playerIcetime")
      .withColumnRenamed("games_played", "playerGamesPlayed")
      .drop("position")
  }

  def mergeLinesWithSkater(skater: Dataset[Row])(lines: Dataset[Row]): Dataset[Row] ={
    lines
      .join(skater,
        col("lineCombo").contains(lower(col("lName")))
        && lines.col("lineTeam") === lower(skater.col("team"))
        && lines.col("situation") === skater.col("situation"),
        "inner"
      )
  }

  def viewPlayerBestLine(df: Dataset[Row]): Dataset[Row] ={
    df
      .groupBy(
        "playerId",
        "lineId",
        "lineCombo",
        "name",
        "team",
        "position",
        "lName",
        "games_played",
        "lineIcetime"
      )
      .agg(
        max($"gameScore".cast("float")/$"playerGamesPlayed".cast("float")).as("avgGameScore"),
        max($"onIce_xGoalsPercentage").as("playerxGoalsPercentage"),
        max("xGoalsPercentage").cast("float").as("linexGoalsPercentage"),
        max($"xGoalsFor".cast("float")/$"games_played".cast("float")).as("lineXGoalsForGame"),
        max($"lineIcetime".cast("float")/$"playerIcetime".cast("float")).as("lineIcePct"),
      )
      .orderBy(desc("linexGoalsPercentage"))
      .drop("lineId", "playerId")
  }

  def filterOutputColumns(df: Dataset[Row]): Dataset[Row] ={
    df
      .select($"lineId",
       $"lineCombo",
       $"team",
       $"situation",
       $"name",
       $"playerId",
       $"position",
       $"gameScore",
       $"games_played")
  }
}
