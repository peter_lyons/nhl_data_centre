package nhl.DailyFaceoff

import config.ConfigLoader._
import nhl.SparkEnvironment.SparkEnvironment
import nhl.NhlApiFullPlayerIdList._

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.browser.Browser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import net.ruippeixotog.scalascraper.model._
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row

object DailyFaceoff extends NhlApiFullPlayerIdList with SparkEnvironment with App{
  // This object will load all the current lines for a given team
  // scraped from dailyfaceoff

  // get Base URL
  val config = loadConfig()
  val dailyFaceInputPath: String = config.getString("daily-faceoff.input.daily-faceoff-template")

  // Load nhl team data
  val (nhlTeams, nhlTeamsApiPath) = getNhlTeamInformation()

  // Create dailyfaceoff links
  val teamList: List[String] = nhlTeams.select("teamName").rdd.map(r => r(0).toString()).collect().toList
  val teamLinks: List[String] = teamList.map(t => dailyFaceInputPath.replace("X", 
    t.toLowerCase.replace(" ", "-").replace("é","e").replace(".","")))

  // things like PP1, L1, etc.
  val dailyFaceoffTableElements: Seq[String] = Seq("", "")

  println(s"Input Path: $dailyFaceInputPath")
  //nhlTeams.show(10, false)
  //println(teamList.mkString)
  //println(teamLinks.mkString("\n"))

  // url will then be reached by headless browser
  val browser = JsoupBrowser()
  val page = browser.get("https://www.dailyfaceoff.com/teams/toronto-maple-leafs/line-combinations/")
  val htmlElement = page.body >> elementList(".team-line-combination-wrap")

  // table "forwards"
  val htmlRows = htmlElement >> elementList("#forwards")
  val test = htmlRows >> texts("a span")

  //#LW1 > a > span

  // split rows and table head
  val (tableHead, tableValues) = htmlRows.splitAt(1)

  // extract head
  val tableHeadExtract = tableHead >> texts("th")

  // extract values
  val tableValuesExtract = tableValues >> texts("td")

  // table ""

  println(test)
  print(tableHeadExtract.mkString("\n"))
  print(tableValuesExtract.mkString("\n"))
  /*
  def extractTables(page: browser.DocumentType, tableElement: String): Dataset[Row] = {
  }*/
  // html tables will be parsed to gather line combinations

  // | f1 | f2 | f3 | f4 | d1 | d2 | d3 | pp1 | pp2 | pk1 | pk2 | g | injured |
  // | [,]| etc.. 

  // save output to local folder for now

  // this is probably a good usecase for a spark streaming instance, or an airflow daily site ping
  // there's a "last update" section on the webpage.. check to see if date changed if not scrape
}
