package nhl.PuckIq

import config.ConfigLoader._
import nhl.SparkEnvironment.SparkEnvironment
import org.apache.spark.sql.{Dataset, Row}

/**
  * Idea, use moneypuck lines data to determine player combos (pairs) - generate a Dataset that contains line pairs, duos if you will
  * 
  * Then, use WOWys against the moneypuck dataset
  * 
  * See which combos are 
  * 
  * 1. power couples (thrive together, suffer apart)
  * 2. one-sided relationships (one player significantly better)
  * 3. disaster on ice (big oof combo)
  * 
  * Layer in QoC too.
  * 
  * Does the notion that "level of competition increases in playoffs" hold against regular season data? 
  * might be a challenge with small sample sizes
  */
object PuckIqDownload extends App with SparkEnvironment {
  // import browser data handling
  import spark.implicits._

  // Load the config file
  val puckIqConfig = loadConfig()

  // TODO create override input args to change default filters
  val shiftConfigURL: String = puckIqConfig.getString("puck-iq.input.puck-iq-shift-data") + "?season=20202021"
  val playerConfigUrl: String = puckIqConfig.getString("puck-iq.input.puck-iq-player-data") + "?season=20202021"

  // Create Player and Shift DFs
  val shiftInputDf: Dataset[Row] = shiftConfigURL.getPuckIqData()
  val playerInputDf: Dataset[Row] = playerConfigUrl.getPuckIqData()

  shiftInputDf.printSchema()
  shiftInputDf.show(10, false)
  playerInputDf.printSchema()
  playerInputDf.show(10, false)

  /**
    * Extended String Methods for puckIq url -> dataset
    *
    * @param url String pointing to dowloadable csv
    */
  implicit class PuckIqUrl(url: String){
    /**
      * Return Dataset from PuckIq query
      *
      * @param url input URL formatted to the download csv link
      * @return Dataset[Row]
      */
    def getPuckIqData(): Dataset[Row] = {
  
      // load URL
      val content = scala.io.Source.fromURL(url).mkString
  
      // parse string
      val splitContent= content.split("\n").filter(_ != "")
  
      // convert to dataframe
      val rdd = sc.parallelize(splitContent)
      val oneCol = rdd.toDS()
  
      // return parsed Df
      spark.read.option("header", true).option("inferSchema",true).csv(oneCol)
    }
  }

  // seems that the data is always contained in a woodwowy-results

  // parse the results

  // dataframe operations.


  //---------------------------------- link stuff -------------------------------//
  // player by season
  //"players/8476853/download?player=8476853&amp;season=all&amp;tier=&amp;team=&amp;group_by=player_season_team"

  //div id: "woodwowy-results"

  // player comps
  // http://puckiq.com/woodwowy?player=8475166&teammate=8477939&season=20202021&from_date=&to_date=
}
