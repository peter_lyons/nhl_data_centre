package nhlDataQuality
import com.amazon.deequ.VerificationSuite
import com.amazon.deequ.constraints.ConstraintStatus
import com.amazon.deequ.checks.{Check, CheckLevel, CheckStatus}
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row

object SalaryValidation {
  def runSalaryDfVerificationSuite(df: Dataset[Row]){
    val verificationResult = VerificationSuite()
      .onData(df)
      .addCheck(
        Check(CheckLevel.Error, "salary Db unit test")
        .isComplete("playerName")
        .isUnique("playerId")
        .isComplete("playerId")
      )
      .run()

    if (verificationResult.status == CheckStatus.Success) {
      println("The data passed the test, everything is fine!")
    } else {
      println("We found errors in the data:\n")

      val resultsForAllConstraints = verificationResult.checkResults
        .flatMap { case (_, checkResult) => checkResult.constraintResults }

      resultsForAllConstraints
        .filter { _.status != ConstraintStatus.Success }
        .foreach { result => println(s"${result.constraint}: ${result.message.get}") }
    }
  }
}
