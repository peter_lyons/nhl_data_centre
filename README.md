# NHL Data Centre
- Peter Lyons
- May 28, 2021

## Preface
The idea behind this is to create a personal data engineering repository that uses a number of different techniques to gather, transform and store data. It just so happens that I like hockey, and there's lots of fragmented data available out there.

Working in a large corporation, there's a lot of various parts of the data engineering process that one might not necessarily be exposed to, or have much time for hands on practice with certain tools. Whether it be starting a project from the most basic template available vs. already having a pre-configured project template to clone from a repo. Or setting up a jenkins server and configs vs just "using it". What I hope to accomplish here is to get into the weeds, get stuck and hopefully learn a lot in the process.

## Development Environment
To begin, my IDE of choice for this would be VSCode with Scala Metals for local build/test. Metals uses maven, so this works perfectly. I started with "creating a new scala project" in Metals, and chose a basic maven template. It's a barebones template that gives me the freedom to add layers as time goes on. One slight hiccup is that I'm using apple silicon, so there were a few extra steps in getting things like JRE/JDK/Spark etc.. set up.

One of the first priorites was to configure the Maven pom.xml file to support build profiles, and add all the dependencies required for Metals/bloop. The next dependencies to be added were for spark, redis, scalascaper, and scalajhttp.

For data storage I have a Redis instance running on a raspberry pi on my network, with plans to add PostgresSql, Airflow and Jenkins for continuous deployment and scheduling.

What I like about this setup is the ability to rapidly test via Metals/Bloop continuous building. As long as any scala object extends App, which contains the main method, then you can run that object via the built in VSCode debugger.

Then, when everything is good to go, I can package through commandline Maven args and specify the build profile for a particular pipeline, and it will build with the main class as defined in the profile properties. It can also be overriden by passing in a different class arg when invoking through spark-submit.

## Draft Data Pipeline

### Data Acquisition
- scalascraper for web scraping
- scalaj for JSON APIs 

### Data Transformation
- Spark for data transformation, with plans to set up a spark stream for live game data.
- Eventually, if time permits, I'd like to set up some python modules with some ML libraries to see how well it integrates within the majority Scala project

### Data Quality
- Data quality with Deequ
  - A straightforward library available through mvnrepository created by aws labs to run diagnostics on dataframes/tabular data formats
  - Can be used to check comptleteness, uniqueness, skew etc.. on various columns. Job can be configured to succeed/fail based on result of these deequ tests.

### Code Quality
- Manual unit tests
  - Something that I'm admittedly not very frequent at doing, mocking up data to test the individual functions.

### Data Storage
- Redis (on raspberry pi for now)
  - A key-value store system that resides in memory, the instance is up and running on my network pi. The plan is to use Redis in conjunction with spark-streaming. Redis-spark package on mvn works well with Scala, but I've had issues getting the same result with pySpark.
- PostgresSQL (ditto)
  - Plan is also to implement on the pi, but that's a future development.

### Build Automation
- Jenkins
  - This poor raspberry pi is getting a workout, in addition to piHole, minidlna, redis, and more, Jenkins will be set up to act as a build, test and deployment tool

### Job Scheduling
- Airflow
  - I've come to enjoy using airflow, another great tool for scheduling the runtime of different jobs. For this project I'm planning to run the server on the pi, of course, and put together some airflow scripts (python) to execute a daily job for ETL of moneypuck data.

### Deployment
- Docker container on pi

## Data Feeds
- NHL API (https://gitlab.com/dword4/nhlapi/-/blob/master/stats-api.md)
- Moneypuck (http://moneypuck.com/)
- PuckPedia (https://puckpedia.com/)

## Resources
Sections I found particularly helpul.

### NHL Stats
- https://gitlab.com/dword4/nhlapi/-/blob/master/stats-api.md

### Scalaj HTTP
- https://github.com/scalaj/scalaj-http

### Scalascraper
- https://github.com/ruippeixotog/scala-scraper

#### Maven Pom
- https://www.tutorialspoint.com/maven/maven_build_profiles.htm
- https://www.baeldung.com/maven-profiles

### Maven Assembly
- https://maven.apache.org/plugins/maven-assembly-plugin/assembly.html
- https://medium.com/@kasunpdh/using-the-maven-assembly-plugin-to-build-a-zip-distribution-5cbca2a3b052

### Redis
- https://thisdavej.com/how-to-install-redis-on-a-raspberry-pi-using-docker/

### Postgres
- https://opensource.com/article/17/10/set-postgres-database-your-raspberry-pi

### Data Quality
- https://github.com/awslabs/deequ

### Project Documentation
- https://docs.scala-lang.org/overviews/scaladoc/for-library-authors.html

### Configuration
- https://medium.com/@ramkarnani24/reading-configurations-in-scala-f987f839f54d
- https://github.com/lightbend/config

### VSCode Launcher Settings
